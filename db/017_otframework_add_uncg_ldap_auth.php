<?php

/**
 * Add UNCG Ldap auth adapter
 * 
 */

class Db_017_otframework_add_uncg_ldap_auth extends Ot_Migrate_Migration_Abstract
    {
        public function up($dba)
        {        
            
            /*
             $query = "INSERT INTO `" . $this->tablePrefix . "tbl_ot_auth_adapter` 
             
                        (`adapterKey`, `class`, `name`, `description`, `enabled`, `displayOrder`) 
                    VALUES 
                        (
                            'uncgldap', 
                            'Internal_Auth_Adapter_Uncgldap_Uncgldapall', 
                            'UNCG LDAP (All Users)', 
                            'LDAP login for all UNCG Users', 
                            '0', 
                            '3'
                        );";

            $dba->query($query);
            */
               
            $query = "TRUNCATE `" . $this->tablePrefix . "tbl_ot_auth_adapter`;";

            $dba->query($query);
            
            $query = "INSERT INTO `" . $this->tablePrefix . "tbl_ot_auth_adapter` 
                (`adapterKey`, `class`, `name`, `description`, `enabled`, `displayOrder`)
                VALUES
                ('local', 'Ot_Auth_Adapter_Local', 'Local Auth', 'Authentication using a local ID and Password created by the user.', 1, 1),
                ('uncgshib', 'Ot_Auth_Adapter_Uncgshibboleth', 'UNCG Enterprise Auth', 'Authentication using UNCG Enterprise Authentication (Shibboleth)', 1, 2),
                ('uncgldap', 'Internal_Auth_Adapter_Uncgldap_Uncgldapfacstaff', 'UNCG LDAP (Employees)', 'LDAP login for UNCG Employees', 1, 3);";

            $dba->query($query);
            

        }

        public function down($dba) 
        {  


        }

    }