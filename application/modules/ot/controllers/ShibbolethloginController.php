<?php
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file _LICENSE.txt.
 *
 * This license is also available via the world-wide-web at
 * http://itdapps.ncsu.edu/bsd.txt
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to itappdev@ncsu.edu so we can send you a copy immediately.
 *
 * @package    Login_IndexController
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of
 *             Information Technology
 * @license    BSD License
 * @version    SVN: $Id: $
 */

/**
 * Allows the user to log in and log out of the application, as well as signup
 * for new accounts and reset passwords.
 *
 * @package    Login_IndexController
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of
 *             Information Technology
 *
 */
class Ot_ShibbolethloginController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $req = new Zend_Session_Namespace(Zend_Registry::get('siteUrl') . '_request');
        
        $loginOptions = Zend_Registry::get('applicationLoginOptions');
        
        if (Zend_Auth::getInstance()->hasIdentity()) {
            if (isset($req->uri) && $req->uri != '') {
                $uri = $req->uri;

                $req->unsetAll();

                $this->_helper->redirector->gotoUrl($uri);
            } else {
                $this->_helper->redirector->gotoRoute(array(), 'default', true);
            }
        }
        
        if(isset($_SERVER["REDIRECT_REDIRECT_eppn"])) {
            /*
            echo '<pre>';
            print_r($_COOKIE);
            print_r($_SESSION);
            print_r($_SERVER);
            die('blahblah');
            */
            $httpCookiePieces = explode('; ', $_SERVER['HTTP_COOKIE']);
            
            foreach($httpCookiePieces as $piece) {
                
                if(strrpos($piece, '_shibsession_', -strlen($piece)) !== FALSE) {
                    
                    $shibSessionPiece = explode('=', $piece);
                    
                    $thisUncgShibSessionNamespace = new Zend_Session_Namespace('thisUncgShibSession');
                    $thisUncgShibSessionNamespace->value = $shibSessionPiece[0];
                    
                }
                
            }
            
            $affiliation = strtolower(substr($_SERVER["REDIRECT_REDIRECT_affiliation"],0,strpos($_SERVER["REDIRECT_REDIRECT_affiliation"], '@')));
            
            $shibOptions = Zend_Registry::get('shibboleth');
            
            if(!empty($allowedRoles)) {
                $allowedRoles = explode(',', $shibOptions['permittedAffiliations']);
                
                if (!in_array($affiliation, $allowedRoles)) {
                
                    $this->_helper->messenger->addError('UNCG Authentication Failed: Must be one of these affiliations/roles: ' . $shibOptions['permittedAffiliations']);
                    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'index', 'action' => 'error'), 'default', true);
                    
                }
                
                $this->_helper->messenger->addSuccess('UNCG Authentication Success: Affiliation/role list: ' . $shibOptions['permittedAffiliations']);
                
            }
            
            $username = strtolower(substr($_SERVER["REDIRECT_REDIRECT_eppn"],0,strpos($_SERVER["REDIRECT_REDIRECT_eppn"], '@')));
            $emailAddress = strtolower($_SERVER["REDIRECT_REDIRECT_eppn"]);
            $sn = $_SERVER["REDIRECT_REDIRECT_sn"];
            $givenName = $_SERVER["REDIRECT_REDIRECT_givenName"];    
            $realm = 'uncgshib';



            $class = new stdClass();
            $class->username = $username;
            $class->firstName = $givenName;
            $class->lastName = $sn;
            $class->emailAddress = $emailAddress;
            $class->timezone = 'America/New_York';
            $class->realm    = $realm;

            $result = new Zend_Auth_Result(true, $class, array());    

            $accountDetails = $result->getIdentity();

            $auth = Zend_Auth::getInstance();
            //$authRealm->realm = $realm;    

            $account     = new Ot_Model_DbTable_Account();
            $thisAccount = $account->getByUsername($username, $realm);

            if (is_null($thisAccount)) {

                $password = $account->generatePassword();

                $acctData = array(
                    'username'  => $username,
                    'password'  => md5($password),
                    'realm'     => $realm,
                    'role'      => $this->_helper->configVar('newAccountRole'),
                    'lastLogin' => time(),
                );

                $identity = $auth->getIdentity();

                if (isset($identity->firstName)) {
                    $acctData['firstName'] = $identity->firstName;
                }

                if (isset($identity->lastName)) {
                    $acctData['lastName'] = $identity->lastName;
                }

                if (isset($identity->emailAddress)) {
                    $acctData['emailAddress'] = $identity->emailAddress;
                }

                if ($loginOptions['generateAccountOnLogin'] != 1) {
                    $auth->clearIdentity();
                    $authAdapter->autoLogout();
                    throw new Ot_Exception_Access('msg-error-createAccountNotAllowed');
                }

                if (isset($accountDetails->firstName)) {
                    $acctData['firstName'] = $accountDetails->firstName;
                }

                if (isset($accountDetails->lastName)) {
                    $acctData['lastName'] = $accountDetails->lastName;
                }

                if (isset($accountDetails->emailAddress)) {
                    $acctData['emailAddress'] = $accountDetails->emailAddress;
                }

                if (isset($accountDetails->timezone)) {
                    $acctData['timezone'] = $accountDetails->timezone;
                }

                $accountId = $account->insert($acctData);

                $thisAccount = $account->getByAccountId($accountId);

            } else {

                // update last login time and details if they are available from the auth adapter
                $data = array(
                    'accountId' => $thisAccount->accountId,
                    'lastLogin' => time()
                );

                if (isset($accountDetails->firstName)) {
                    $data['firstName'] = $accountDetails->firstName;
                }

                if (isset($accountDetails->lastName)) {
                    $data['lastName'] = $accountDetails->lastName;
                }

                if (isset($accountDetails->emailAddress)) {
                    $data['emailAddress'] = $accountDetails->emailAddress;
                }

                if (isset($accountDetails->timezone)) {
                    $data['timezone'] = $accountDetails->timezone;
                }

                $account->update($data, null);
            }

            $auth->getStorage()->write($thisAccount);

            $loggerOptions = array(
                'accountId'     => $thisAccount->accountId,
                'role'          => (is_array($thisAccount->role)) ? implode(',', $thisAccount->role) : $thisAccount->role,
                'attributeName' => 'accountId',
                'attributeId'   => $thisAccount->accountId,
            );

            $this->_helper->log(Zend_Log::INFO, 'User ' . $username . ' logged in via the shibbolethlogin controller', $loggerOptions);

            if (isset($req->uri) && $req->uri != '') {
                $uri = $req->uri;

                $req->unsetAll();

                return $this->_helper->redirector->gotoUrl($uri);
            } else {
                return $this->_helper->redirector->gotoRoute(array(), 'default', true);
            }
                
        } else {
                
            if (count($result->getMessages()) == 0) {
                $this->_helper->messenger->addError('msg-error-invalidUsername');
            } else {
                foreach ($result->getMessages() as $m) {
                    $this->_helper->messenger->addInfo($m);
                }
            }
        }
    }
        
        
    public function errorAction()
    {
        $this->_helper->pageTitle('Shibboleth Authentication Error');

    }

}