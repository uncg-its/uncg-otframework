<?php
class Ot_Form_Emailqueuetest extends Zend_Form
{
    public function __construct($options = array())
    {
        parent::__construct($options);

        $this->setAttrib('id', 'emailqueueSearchForm')
             ->setDecorators(array(
                     'FormElements',
                     array('HtmlTag', array('tag' => 'div', 'class' => 'well')),
                     'Form',
             ))
             ->setMethod(Zend_Form::METHOD_POST);

        $to = $this->createElement('text', 'to', array('label' => 'To'));
        $to->setRequired(true)->addFilter('StringTrim');
        
        $from = $this->createElement('text', 'from', array('label' => 'From'));
        $from->setRequired(true)->addFilter('StringTrim');
        
        $subject = $this->createElement('text', 'subject', array('label' => 'Subject'));
        $subject->setRequired(true)->addFilter('StringTrim');
        
        $body = $this->createElement('textarea', 'body', array('label' => 'Body'));
        $body->setRequired(false)
                ->addFilter('StringTrim')
                ->setAttrib('style', 'width: 50%; height: 150px;')
                ->setDescription('HTML or Plain text supported.');

        $this->addElements(array($to, $from, $subject, $body));
            
        $this->setElementDecorators(array(
                  'ViewHelper',
                  array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
                  array('Errors', array('placement' => 'append')),
                  array('Label', array('placement' => 'prepend')),
                  array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
              ));

        $submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
        $submit->setDecorators(array(
                   array('ViewHelper', array('helper' => 'formSubmit')),
                   array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
                   array('HtmlTag', array('tag' => 'div', 'class' => 'clearfix')),
                 ));
        
        $this->addElements(array($submit));

        return $this;

    }
}
