<?php
class Ot_Cronjob_Detectlockfiles implements Ot_Cron_JobInterface
{
    
    public function execute($lastRunDt = null)
    {
        
        $cachePath = APPLICATION_PATH . '/../cache/';

        // get file listing, sorted by oldest first ([0] in the array will be the oldest file from the directory
        $fileListing = array_diff(scandir($cachePath, 1), array('..', '.'));
        
        if(count($fileListing) > 0) {
            
            $lockFileArray = array();
            $oneHourAgo = time() - 3600;
            $helper = new Cst_Model_Helper();
            
            foreach($fileListing as $thisFile) {
                
                // check to see if it's a lock file
                if($helper->endsWith($thisFile, '.lock')) {
                    
                    // check to see if the file was created more than an hour ago
                    $fileTime = filemtime($cachePath . '/' . $thisFile);
                    
                    if($fileTime < $oneHourAgo) {
                        $lockFileArray[$thisFile] = date('Y-m-d @ H:i:s A', $fileTime);    
                    }
                    
                }

            }
        }
        
        // if lock files exist that are older than an hour, then send an email
        if(!empty($lockFileArray)) {
            
            $et = new Ot_Trigger_Dispatcher();
            foreach($lockFileArray as $lockFileName => $lockFileTime) {
                $et->setVariables(array(
                'cronjob'           => 'Ot_Cronjob_Detectlockfiles',
                'subject'           => 'Lock File Problem: ' . $lockFileName . ' (FAILED)',
                'summary'           => 'Lock File "' . $lockFileName . '" has existed for over one hour',
                'details'           => 'Lock File "' . $lockFileName . '" was created: ' . $lockFileTime,
                'timestamp'         => date('Y-m-d @ H:i:s A')
                ));

                $et->dispatch('Cron_Job_Event');
            }
            
        }
        
    }
}