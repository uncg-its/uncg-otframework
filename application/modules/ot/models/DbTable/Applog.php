<?php
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 *
 * This license is also available via the world-wide-web at
 * http://itdapps.ncsu.edu/bsd.txt
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to itappdev@ncsu.edu so we can send you a copy immediately.
 *
 * @package    Ot_Log
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 * @license    http://itdapps.ncsu.edu/bsd.txt BSD License
 * @version    SVN: $Id: $
 */

/**
 * Model to do all interaction for logs for the instance of the application 
 * (not the OT Framework Logging). 
 *
 * @package    Ot_Applog
 * @category   Model
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 *
 */

class Ot_Model_DbTable_Applog extends Ot_Db_Table
{

    protected $_name = 'tbl_ot_applog';
    protected $_primary = 'logId';
    
    
    public function addLogMessage($subject, $message, $priority, $priorityName, $attributeName, $attributeId)
    {
       
        // set things to 0, as this should only be used for models (not actions/views)
        $accountId = '0';
        $role = '0';
        $sid = '0';
        $request = '0';
        $loggedInUser = Zend_Auth::getInstance()->getIdentity();
        if(!empty($loggedInUser)) {
            $accountId = Zend_Auth::getInstance()->getIdentity()->accountId;
        } 
        
        $data = array(
            'accountId'     => $accountId,
            'role'          => $role,
            'request'       => $request,
            'sid'           => $sid,
            'timestamp'     => time(),
            'subject'       => $subject,
            'message'       => $message,
            'priority'      => $priority,
            'priorityName'  => $priorityName,
            'attributeName' => $attributeName,
            'attributeId'   => $attributeId,
        );

        $logId = $this->insert($data);
        
        return $logId;
      
    }
    
}