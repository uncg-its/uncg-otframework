<?php

class Ot_Auth_Adapter_Uncgshibboleth implements Zend_Auth_Adapter_Interface, Ot_Auth_Adapter_Interface
{

    /**
     * Username of the user to authenticate
     *
     * @var string
     */
    protected $_username = '';

    /**
     * Password of the user to authenticate
     *
     * @var string
     */
    protected $_password = '';

    /**
     * Constant for default username for auto-login
     *
     */
    const defaultUsername = '';

    /**
     * Constant for default password for auto-login
     *
     */
    const defaultPassword = '';
    
    /**
     * Constructor to create new object
     *
     * @param string $username
     * @param string $password
     */
    public function __construct($username = self::defaultUsername, $password = self::defaultPassword)
    {
        $this->_username = $username;
        $this->_password = $password;
    }

    /**
     * Authenticates the user passed by the constructor, however in this case we
     * user the WRAP server variable "WRAP_USERID" to get this appropriate username.
     *
     * @return new Zend_Auth_Result object
     */
    public function authenticate()
    {
        // check to see if this is a logout request
        $pos = strpos($_SERVER['REQUEST_URI'], "logout");
        
        if($pos === FALSE) {
            
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $class = new stdClass();
                $class->username = Zend_Auth::getInstance()->getIdentity()->username;
                $class->firstName = Zend_Auth::getInstance()->getIdentity()->firstName;
                $class->lastName = Zend_Auth::getInstance()->getIdentity()->lastName;
                $class->emailAddress = Zend_Auth::getInstance()->getIdentity()->emailAddress;
                $class->timezone = 'America/New_York';
                $class->realm    = 'uncgshib';

                return new Zend_Auth_Result(true, $class, array());
            }
            
            $applicationBaseUrl = preg_replace('/\/login$/', '', $this->_getUrl());

            setrawcookie('UNCGSHIB_referrer', rawurlencode($this->_getUrl()), time()+3600);

            if (!isset($_SESSION["UNCGSHIB_username"])) {
                sleep(1);
                header('location:' . $applicationBaseUrl . '/ot/shibbolethlogin');            
                die('you should never see this message. contact an admin');
            }

            if ($_SESSION["UNCGSHIB_username"] == 'guest') { 
                return new Zend_Auth_Result(false, null, array('UNCG LDAP Authentication Failed: Not Current Faculty or Staff.'));
            }

            $class = new stdClass();
            $class->username = $_SESSION['UNCGSHIB_username'];
            $class->firstName = $_SESSION['UNCGSHIB_givenName'];
            $class->lastName = $_SESSION['UNCGSHIB_sn'];
            $class->emailAddress = $_SESSION['UNCGSHIB_emailAddress'];
            $class->timezone = 'America/New_York';
            $class->realm    = 'uncgshib';

            // now get rid of the session variables we got from SHIB, so they can't be re-used without re-authing to shib again
            unset($_SESSION['UNCGSHIB_username']);
            unset($_SESSION['UNCGSHIB_affiliation']);
            unset($_SESSION['UNCGSHIB_emailAddress']);
            unset($_SESSION['UNCGSHIB_sn']);
            unset($_SESSION['UNCGSHIB_givenName']);
            /*
            echo '<pre>';
            print_r($_COOKIE);
            print_r($_SESSION);
            die('blahblah');
            */
            return new Zend_Auth_Result(true, $class, array());
        } else {
            
            $this->autoLogout();
        }
    }

    /**
     * Gets the current URL
     *
     * @return string
     */
    protected function _getURL()
    {
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";

        $protocol = substr(
            strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")
        ) . $s;

        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);

        return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
    }

    /**
     * Setup this adapter to autoLogin
     *
     * @return boolean
     */
    public static function autoLogin()
    {
        return true;
    }

    /**
     * Logs the user out
     *
     */
    public static function autoLogout()
    {
        $thisUncgShibSessionNamespace = new Zend_Session_Namespace('thisUncgShibSession');
        
        // kill the current shib cookie that we got during login until it's dead
        unset($_COOKIE[$thisUncgShibSessionNamespace->value]);
        setcookie($thisUncgShibSessionNamespace->value, NULL, time()-3600, '/');
        
        $shibOptions = Zend_Registry::get('shibboleth');
        $logOutUrl = $shibOptions['logoutUrl'];
        header("Location: $logOutUrl");   
    }

    /**
     * Flag to tell the app where the authenticaiton is managed
     *
     * @return boolean
     */
    public static function manageLocally()
    {
        return false;
    }
    
    /**
     * flag to tell the app whether a user can sign up or not
     *
     * @return boolean
     */
    public static function allowUserSignUp()
    {
        return false;
    }

}