<?php

/**
 *
 *
 * @package   Ot_Application_Resource_Shibboleth
 * @category  Library
 * @copyright Copyright (c) 2015 UNCG ITS
 *
 */

class Ot_Application_Resource_Shibboleth extends Zend_Application_Resource_ResourceAbstract
{
    protected $_logoutUrl = '';

    protected $_permittedAffiliations = '';
    
    public function setLogoutUrl($val)
    {
        $this->_logoutUrl = $val;
    }
    
    public function setPermittedAffiliations($val)
    {
        $this->_permittedAffiliations = $val;
    }
    
    public function init()
    {
        $thisConfig = array(
            'logoutUrl' => $this->_logoutUrl,
            'permittedAffiliations' => $this->_permittedAffiliations,
        );
        
        Zend_Registry::set('shibboleth', $thisConfig);
    }
}