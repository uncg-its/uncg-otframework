<?php

class Ot_Application_Resource_Customapplog extends Zend_Application_Resource_ResourceAbstract
{
    protected $_logFileArray = array(); // array of log files and file paths
    
    public function setLogFileArray($val)
    {
        $this->_logFileArray = $val;
    }
      
    public function init()
    {
                       
        $thisConfig = array(
            'logFileArray' => $this->_logFileArray,
        );
        
        Zend_Registry::set('customapplog', $thisConfig);
    }
}