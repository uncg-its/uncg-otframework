<?php

class Ot_Application_Resource_Cronoverride extends Zend_Application_Resource_ResourceAbstract
{
    
    protected $_jobArray = ''; // local path to source file
    
    public function setJobArray($val)
    {
        $this->_jobArray = $val;
    }
      
    public function init()
    {
                       
        $resourceConfig = array(
            'jobArray'          => $this->_jobArray,
        );
        
        Zend_Registry::set('cronoverride', $resourceConfig);
    }
}